 
package ru.myGelicopter;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;


public class Player {
    
    public static final int MAX_V = 60;
    public static final int MAX_TOP = -35;// -100;
    public static final int MAX_DOWN = 605;
    
    
    Image img = new ImageIcon("res/Player2.gif").getImage();
    
    public Rectangle getRect(){
        return new Rectangle((int)x,(int) y, 269, 83);
    }
    
    double v = 1; // speed
    double dv = 0; //speeding
    //double gr = 0.1; // gravity
    //double agr = -0.7; // antigravity
    static double dgr = 1.25; //1.98; // anti/graviting
    double diff_y = 1; //gr * dgr;
    // начальное положение и расстояние: 
    int s = 0;
    double x = 100;
    double y = 280;
    
    
    double fon1 = -91;
    double fon2 = 1573;
    double fon3 = -1755;
    
    int baH = 0; //кол-во столкновений
    
    public void move(){
        s += v;
        v += dv;
        if(v >= MAX_V)v = MAX_V;
      y += diff_y + 1.1; // +0.1-gravity== 1,1-лучше!
        if (y <= MAX_TOP)y = MAX_TOP;
        if (y >= MAX_DOWN){
            y = MAX_DOWN;
            v = 0;
        }
        if (fon2 - v <= -91){
            fon1 = -91;
            fon2 = 1573;
        }else if (fon1 - v > -90){
            fon3 = fon1-1664;    
        }else
            fon1 -= v;
            fon2 -= v;//fon3 -= v;         
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_RIGHT){
            dv =0.5;
        }
         if (key == KeyEvent.VK_LEFT){
            dv =-0.2;
        }
         if (key == KeyEvent.VK_UP){
             //dgr *= -1;
             diff_y += - dgr-6;
        }
         if (key == KeyEvent.VK_DOWN){
             diff_y += dgr + 4;
         }
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT){
            dv = 0;
        }
        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN){
            y += diff_y /= 10+1.1;
        }    
    }
    
}

