
package ru.myGelicopter;

//import java.applet.AudioClip; // проверить при случае
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
//import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.sound.sampled.spi.AudioFileReader;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/** Основное поле игры-
 * содержит методы:  run(), testCollisionWithSkyObjects(), testWin()-приехали,
 * paint(), actionPerformed().
 * а так же потоки, таймер, старты и обработка клав - class MyKeyAdapter.
 * @author ric с подсказки java2e.ru
 */
public class Sky extends JPanel implements ActionListener, Runnable{
    
    Timer mainTimer = new Timer(20, this);
  
    Image img = new ImageIcon("res/bg_sky.jpg").getImage();
  //  Image img = new ImageIcon(getClass().getClassLoader().getResource("res/bg_sky.jpg")).getImage();
    
    Player p = new Player();
    
    Random rand = new Random();
    
    //Thread audioThread = new Thread(new AudioThread());
    
    Thread skyobjectsFactory = new Thread(this);
    List<SkyObjects> skyobjct = new ArrayList<SkyObjects>();
    
    
    public Sky(){
        mainTimer.start();
        skyobjectsFactory.start();
       // audioThread.start();
        addKeyListener(new MyKeyAdapter());
        setFocusable(true);
    }

    @Override
    public void run() {
        
       while(true){
             
         try {    
             Thread.sleep(rand.nextInt(2500));
             skyobjct.add(new SkyObjects(1660/*|-1660*/, rand.nextInt(600), rand.nextInt(5)/*rand.nextInt(-2),*/ // минус не считает
                     ,rand.nextInt(40),this));
        } catch (InterruptedException ex) {
            Logger.getLogger(Sky.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    private void testCollisionWithSkyObjects() {
        Iterator<SkyObjects> i = skyobjct.iterator();
        while(i.hasNext()){
            SkyObjects s = i.next();
            if (p.getRect().intersects(s.getRect())){ //  действие при столкновении
                i.remove();
                p.baH ++;
                String  st = "БаБАХ-!!!";
                JOptionPane.showMessageDialog(this, "Удар "+ p.baH+" с "+s, st, p.baH /*skyobjct.size()*/); 
                
                if(p.baH >= 3){
                    
                   System.exit(1); //тут сделать выход во вход))
               // }else{
                }return;
            }
        }
    } 

    private void testWin() {
        if(p.s > 30000){
            JOptionPane.showMessageDialog(this, "Расстояние "+p.s+" Скорость "+ (190/Player.MAX_V)*p.v 
                    +" км/ч", "Отлично!", WIDTH); //,(Icon)p.img);
            System.exit(0);
        }
    }
    
    private class MyKeyAdapter extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e) {
           p.keyPressed(e); 
        }
        @Override
        public void keyReleased(KeyEvent e) {
           p.keyReleased(e); 
        }
    }
    @Override
    public void paint (Graphics g){
        g = (Graphics2D) g;
        g.drawImage(img, (int) p.fon1, 0, null);
        g.drawImage(img, (int) p.fon2, 0, null);
        //g.drawImage(img, (int) p.fon3, 0, null);
        g.drawImage(p.img,(int) p.x,(int) p.y, null);
        
        
        int v = (int) ((190/Player.MAX_V)* p.v); // макс. км/ч в 1 пикселе * скорость в пикселях = текущая скорость в км/ч
        int h = (int) ((Player.MAX_DOWN) - p.y)/10;
        g.setColor(Color.red);
        Font font = new Font("Arial",Font.PLAIN, 10);
        g.setFont(font);
        g.drawString("Высота"+h+"м.",    (int) p.x+6, (int) p.y+52);
        g.drawString("Скорость"+v+"км/ч",(int) p.x+6, (int) p.y+65);
        g.drawString("obj " +skyobjct.size(),(int) p.x+210,(int) p.y+40); // индикатор числа обьектов в поле
        
        Iterator<SkyObjects> i = skyobjct.iterator(); // удаление обьектов вне поля
        while(i.hasNext()){
            SkyObjects s = i.next();
            if (s.x< 2400 && s.x > -2400){
                s.move();   // тут же их поведение и перемещение
                g.drawImage(s.img,s.x, s.y, null);
//                g.drawString("Высота"+h+"м.",    (int) s.x+6, (int) s.y+52); // взять их параметры
                g.drawString("Скорость"+v+"км/ч",(int) s.x+6, (int) s.y+65);
                g.setColor(Color.getHSBColor(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                g.fillOval(s.x+ 126, s.y+28, 8, 8);
            }else{
                i.remove();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        p.move();
        repaint();
        testCollisionWithSkyObjects();
        testWin();
        //System.out.println(skyobjct.size()); // проверка удаления обьектов -> т.е. размера списка в текущий момент.(реализовано показом кол-ва перед кабиной игрока)
        
    }
}
